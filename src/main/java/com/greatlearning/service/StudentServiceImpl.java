package com.greatlearning.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatleaning.model.Student;
import com.greatlearning.dao.StudentDao;

@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	StudentDao studentDaoImpl;

	@Override
	public List<Student> findAll() {
		return studentDaoImpl.findAll();
	}

	@Override
	public Student findById(int id) {
		return studentDaoImpl.findById(id);
	}

	@Override
	public void save(Student student) {
		studentDaoImpl.save(student);
	}

	@Override
	public void deleteById(int id) {
		studentDaoImpl.deleteById(id);
	}

}
